


create_clock -name gtrefclk0_in -period 4.167 [get_pins GTHREFCLK_GEN/O]


create_clock -name GT_TX_WORD_CLK[0] -period 4.167 [get_pins TXUSRCLK_GEN_240.tx_usrclk_bufg1/O]
create_clock -name GT_TX_WORD_CLK[0] -period 4.167 [get_pins TXUSRCLK_GEN_120.tx_usrclk_bufg1/clk_out1]
create_clock -name GT_TX_WORD_CLK[0] -period 4.167 [get_pins TXUSRCLK_GEN_160.tx_usrclk_bufg1/clk_out1]

create_clock -name gt0_rxusrclk -period 4.167 [get_pins rx_usrclk_bufg1/O]


set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 2


create_clock -name clk200m -period 5 [get_pins clk200m_i/O]
#set_property SEVERITY {Warning} [get_drc_checks REQP-44]
#set_property SEVERITY {Warning} [get_drc_checks REQP-46]


set_property PACKAGE_PIN L25 [get_ports sma_p]
set_property IOSTANDARD LVCMOS25 [get_ports sma_p]
set_property PACKAGE_PIN K25 [get_ports sma_n]
set_property IOSTANDARD LVCMOS25 [get_ports sma_n]

set_property PACKAGE_PIN Y23 [get_ports io_p]
set_property IOSTANDARD LVCMOS25 [get_ports io_p]
set_property PACKAGE_PIN Y24 [get_ports io_n]
set_property IOSTANDARD LVCMOS25 [get_ports io_n]

set_property PACKAGE_PIN AD12 [get_ports SYSCLK_P]
set_property IOSTANDARD LVDS [get_ports SYSCLK_P]
set_property PACKAGE_PIN AD11 [get_ports SYSCLK_N]
set_property IOSTANDARD LVDS [get_ports SYSCLK_N]



set_property PACKAGE_PIN G3 [get_ports {gthrxn_in[0]}]

set_property PACKAGE_PIN H5 [get_ports {gthrxn_in[1]}]

set_property PACKAGE_PIN K5 [get_ports {gthrxn_in[2]}]

set_property PACKAGE_PIN F5 [get_ports {gthrxn_in[3]}]


set_property PACKAGE_PIN J7 [get_ports mgtrefclk0_x0y8_n]
set_property PACKAGE_PIN J8 [get_ports mgtrefclk0_x0y8_p]

#set_property PACKAGE_PIN L7 [get_ports mgtrefclk1_x0y8_n]
#set_property PACKAGE_PIN L8 [get_ports mgtrefclk1_x0y8_p]

set_false_path -from [get_clocks clk_out1_clk_wiz_4] -to [get_clocks gt0_rxusrclk]
set_false_path -from [get_clocks clk_out1_clk_wiz_4] -to [get_clocks clk_out1_clk_wiz_3]
set_false_path -from [get_clocks clk_out1_clk_wiz_4] -to [get_clocks GT_TX_WORD_CLK[0]]

#set_property PACKAGE_PIN W27 [get_ports clk40m_out_p]
#set_property IOSTANDARD LVDS_25 [get_ports clk40m_out_p]
#set_property PACKAGE_PIN W28 [get_ports clk40m_out_n]
#set_property IOSTANDARD LVDS_25 [get_ports clk40m_out_n]

#set_property PACKAGE_PIN AE20 [get_ports SI5324_RST_N]
#set_property IOSTANDARD LVCMOS25 [get_ports SI5324_RST_N]
#set_property PACKAGE_PIN L21 [get_ports PL_IIC2_SDA]
#set_property IOSTANDARD LVCMOS25 [get_ports PL_IIC2_SDA]

#set_property PACKAGE_PIN K21 [get_ports PL_IIC2_SCL]
#set_property IOSTANDARD LVCMOS25 [get_ports PL_IIC2_SCL]
#set_property PACKAGE_PIN P23 [get_ports PL_IIC_BUFFER_EN]
#set_property IOSTANDARD LVCMOS25 [get_ports PL_IIC_BUFFER_EN]
 
   
 
