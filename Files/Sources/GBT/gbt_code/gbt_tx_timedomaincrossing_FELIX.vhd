-------------------------------------------------------------------------------
-- Based on GBT-FPGA project v3
-- Added by K. Chen  @ Dec. 2014, purpose is to make TX phase adjustable
-- And make sure the scrambler_enable has fixed latency compared to TTC40M, 
-- even the 40M is sampled by 240M at the rising edge
-------------------------------------------------------------------------------



library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;
 
use work.FELIX_gbt_package.all;

entity gbt_tx_timedomaincrossing_FELIX is
  generic

(
channel             : integer   := 0
);
  port
    (
      
     -- Scrambler_Enable	: in  std_logic;
      Tx_Align_Signal   : out std_logic;
      TX_TC_METHOD : in std_logic;
      TX_LATOPT_TC     : in  std_logic;
      
      TC_EDGE : in std_logic;
      
	  TX_TC_DLY_VALUE  : in std_logic_vector(2 downto 0);  
      
      TX_WORDCLK_I      : in  std_logic;
      TX_RESET_I        : in  std_logic;
      TX_FRAMECLK_I     : in  std_logic
      
      --TX_ISDATA_SEL_I   : in  std_logic;
    
      
   );
end gbt_tx_timedomaincrossing_FELIX;


architecture Behavior of gbt_tx_timedomaincrossing_FELIX is   

  --signal tx_frameclk_i_shifted, tx_frameclk_i_shifted_p : std_logic;
  signal fsm_rst                                : std_logic := '0';
  signal TX_FRAMECLK_I_4r, TX_FRAMECLK_I_5r,TX_FRAMECLK_I_2r,TX_FRAMECLK_I_r       : std_logic;
  signal cnt, TX_TC_DLY_VALUE_i                                    : std_logic_vector(2 downto 0) :="000";
  signal TX_RESET_FLAG_clr, TX_RESET_FLAG,TX_RESET_r,TX_RESET_2r ,pulse_rising,pulse_rising_r,pulse_falling_r,pulse_falling   : std_logic;
  
 
begin                
--=================================================================================================--

   --==================================== User Logic =====================================--


 
 

  -----------------------------------------------------------------------------
  -- Alignment signal for TX GearBox
    -----------------------------------------------------------------------------

  TX_TC_DLY_VALUE_i <= TX_TC_DLY_VALUE when TX_DLY_SW_CTRL='1' else TX_TC_DLY_VALUE_package;



    process(TX_WORDCLK_I)
    begin
      
      if TX_WORDCLK_I'event and TX_WORDCLK_I='0' then
        
       TX_FRAMECLK_I_4r <= TX_FRAMECLK_I;
       TX_FRAMECLK_I_5r <= TX_FRAMECLK_I_4r;
       
       
    end if;
    end process;   
    
    process(TX_WORDCLK_I)
    begin
      
      if TX_WORDCLK_I'event and TX_WORDCLK_I='1' then
        
       TX_FRAMECLK_I_r <= TX_FRAMECLK_I;
       TX_FRAMECLK_I_2r <= TX_FRAMECLK_I_r;
       TX_RESET_r <= TX_RESET_I;
       TX_RESET_2r <= TX_RESET_r;
       if TX_RESET_r='1' and TX_RESET_2r='0' then
           TX_RESET_FLAG <='1';
       elsif  TX_RESET_FLAG_clr='1' then
           TX_RESET_FLAG <='0';
       end if;
       pulse_rising <=TX_FRAMECLK_I_r and (not TX_FRAMECLK_I_2r);
       pulse_falling <= TX_FRAMECLK_I_4r and (not TX_FRAMECLK_I_5r);
       pulse_rising_r <= pulse_rising;
       pulse_falling_r <= pulse_falling;
       if (TX_TC_METHOD='1' or TX_RESET_FLAG='1') then
          if TC_EDGE='0' then
             TX_RESET_FLAG_clr <= pulse_rising; 
          else
             TX_RESET_FLAG_clr <= pulse_falling_r;
          end if;

       else 
            TX_RESET_FLAG_clr <='0';
            
       end if;
       end if;
   end process;         
            
process(TX_WORDCLK_I)
begin
              
      if TX_WORDCLK_I'event and TX_WORDCLK_I='1' then 
        if TX_RESET_FLAG_clr='1' then
             cnt <= TX_TC_DLY_VALUE_i;
             if TX_TC_DLY_VALUE_i = "011" then
               Tx_Align_Signal <='1';
             else
               Tx_Align_Signal <='0';
             end if;
        else
              
        case cnt is
          when "000" =>
            cnt <="001";
            fsm_rst <='0';
            Tx_Align_Signal <='0';
          -- tx_frameclk_i_shifted <='1';
          when "001" =>
            cnt <="010";
            fsm_rst <='0';
            Tx_Align_Signal <='0';
          -- tx_frameclk_i_shifted <='1';
          when "010" =>
            cnt <="011";
            fsm_rst <='0';
            Tx_Align_Signal <='1';
          -- tx_frameclk_i_shifted <='0';
          when "011" =>
            cnt <="100";
            fsm_rst <='0';
            Tx_Align_Signal <='0';
          -- tx_frameclk_i_shifted <='0';
          when "100" =>
            cnt <="101";
            fsm_rst <='0';
            Tx_Align_Signal <='0';
          -- tx_frameclk_i_shifted <='0';
          when "101" =>
            cnt <="000";
            fsm_rst <='0';
            Tx_Align_Signal <='0';
          -- tx_frameclk_i_shifted <='1';
          when others =>
            cnt <="101";
            fsm_rst <='1';
            Tx_Align_Signal <='0';
        end case;
       end if; 
       end if;
    end process;
    
  

  --============--
  -- Scramblers --
  --============--
   
  -- 84 bit scrambler (GBT-Frame & Wide-Bus):
  -------------------------------------------
   

   --=====================================================================================--
end Behavior;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
