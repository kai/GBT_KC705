# GBT_KC705
```diff
- ***Please pay attention to the Caution part.***
```

```diff
+ ***GTX IP in this repo can be used directly on ZC706**
+ ***An example for VC709 GTH: **https://github.com/simpway/GBT-VC709**
+ ***For Ultrascale, TX latency is about 4.1 ns less; RX latency is about 16 ns less.***
+ ***Version for Ultrascale GTH/GTY: to be uploaded before end of 2016.***
```

## Introduction
This project gives an example to the optimized GBT-FPGA on Xilinx KC705 evaluation board. After the optimization, the latency is smaller, the TX and RX GBT modes can be seperately changed on-line without reprogramming, for TX and RX seperately.

For multi-channel design, the ***GBT4FELIX*** repo can be referred to share (if needed) RX side clock resources, for the low fixed latency mode.

For details about the optimization, one can refer to the article https://doi.org/10.1088/1748-0221/12/07/P07011.  

This example support to use a MGTRefClk of 120/240/160 MHz. The TX and RX latency are fixed. The .pdf file in *Docs* shows the details how to use this example.

New projects for front-end will be added: 
- GTX receiver using local MGTRefClk (for CPLL) recovers the TTC clock from RX link. Si5324 on-board cleans it and drive the GTX transmitter (using QPLL, a trick is applied to let QPLL support 4.8 Gbps line rate). Both of transmitter and receiver parts are latency fixed.
- GTX transmitter side uses Xilinx xapp589 digital VCXO to synchronize with RX side.

## Preliminary results
- Latency from TX 40MHz to RX recovered 40MHz.
  + Tx side, around 27.8~32 ns. Latency of the time domain crossing from TX 40M to TX 240M is depends by the phase relationship between these two clocks, but it is fixed.
  + Rx side, around: 43.9 ns for Wide-Bus mode, 56.4 ns (without no timing error) for FEC mode. When the online switch is enabled (default configuration, use gbt\_rx\_gearbox\_FELIX\_WBonly.vhd to disable it if you care about this 4.17 ns), Wide-Bus mode is about 48.0 ns.

  |Latency | FEC    | FEC*  |Wide-Bus | Wide-Bus (with online mode switch)|
  |--------|--------|--------|----------|-----------------------------------|
  |TX      | 27.8~32|        |27.8~32  | 27.8~32 |
  |RX      | 56.4   | 52.2   | 43.9     | 48.0/or same with FEC    |
  |TX+RX   | 84.2~88.4| 80~84.2       |71.7~75.9 | 75.9~82/or same with FEC|
  Note: we give 12 ns for the FEC RX decoder, no timing error exist. If 8 ns is given, there is timing error, but the test shows that no error happens. Depents on your FPGA speed, the multi-cycle path setting can be changed.
- Latency from TX 240MHz to RX recovered 240MHz.
  + We consider the latency as from falling edge of Scrambler enable pulse at TX side, to falling edge of Descrambler enable pulse at RX side.
  + Tx side, around 52.87 ns.
  + Rx side, around: 39.7 ns for Wide-Bus mode, 52.20 ns (without no timing error) for FEC mode. When the online switch is enabled (default configuration, use gbt\_rx\_gearbox\_FELIX\_WBonly.vhd to disable it if you care about this 4.17 ns), Wide-Bus mode is about 43.9 ns.

  |Latency | FEC    | FEC* | Wide-Bus | Wide-Bus (with online mode switch)|
  |--------|--------|----------|---------|-----------------------------------|
  |TX      | 52.87  | 52.87         | 52.87   | 52.87   |
  |RX      | 52.2   | 48.0     | 39.7    | 43.9/or same with FEC   |
  |TX+RX   | 105.1  | 100.9    | 92.6     | 96.8/or same with FEC   |
    Note: we give 12 ns for the FEC RX decoder, no timing error exist. If 8 ns is given, there is timing error, but the test shows that no error happens. Depents on your FPGA speed, the multi-cycle path setting can be changed.
- The measured latency will change in a range of about 2 ns when the firmware is changed and recompiled, since no LOC constraint is applied.

## Usage
- Generally, the .tcl script can be used to create the project (Vivado 2016.1).
- Click the SOFT\_TXRST\_GT[0], gth\_tx\_pll\_reset[0], tx\_reset\_i[0], SOFT\_RXRST\_GT[0], rx\_reset\_i[0].
  + For KC705, gth\_tx\_pll\_reset[0] is needed, I had modify code to do it automatically after SOFT\_TXRST\_GT[0]. But for ZC706, this is not needed.
  + For KC705, rx\_reset\_i[0] is not needed. But for ZC706, it is needed.
- Due to the DDR oddeven issue of CDR, the oddeven in VIO may needed to be changed to '1' to get GBT locked. The GTX reset may not help to solve oddeven issue, if the CDR sample at the center of the eye diagram (if RX reference clock is synchronized with the TX side reference clock, this may happen). **A FSM can be added to do it automatically.** 
- Measure the latency from TX 40MHz domain to RX 40MHz domain: use the SMA connector J13/J14.
- Measure the latency from TX 240MHz domain to RX 240MHz domain: use the SMA connector J11/J12.
- The guideline document should be read if you need to change this design for your project.
- To test Wide-Bus mode, set DATA\_MODE\_i to be 0x5.

## Caution
- **When using the v1.0 of KC705, the TXPOLARITY and RXPOLARITY must be changed to '1', since the polarity is reversed on board.** They are in the file *gtwizard\_cpll\_4p8g\_k7\_2016p1\_gt.vhd*.
- **Wide-Bus mode is recommended, if it is not used in radiation enviroment, since the latency can be save for 8.3/12.5 ns.** The default configuration enable online mode switch, use gbt\_rx\_gearbox\_FELIX\_WBonly.vhd to disable it if you want to save 12.5 ns here, **but the multi-cycle constraint should be commented. The top level should directly set DATAMODE to "01".**
- **For RX part, when run-time mode change is used. If the multi-cycle constraint is used, for wide-bus mode, there is possible hidden timing issue. We are still trying to find a better method to apply multi-cycle path for and only for FEC related path. Though we didn't encounter this error even for 24 channels design, if error happens, temporarily one way is to disable the multi-cycle constraint, and neglect the RX FEC path timing error, since the 12.5 ns margin is already given. Another method is also give 12.5 ns for wide-bus mode. To realize it, the SAME_LAT_FOR_WB_FEC in package file should be set to '1'. If you don't need run-time mode change(for most design, this is not needed), the DATAMODE at top level should be fixed "00" or "01", "00" is for FEC mode. For WB mode, you should comment multi-cycle constraint. At this time you can use gbt\_rx\_gearbox\_FELIX.vhd or ~WBonly.vhd, the latter one has a 4.17 ns smaller latency.**
- For TX part, no multi-cycle path is needed.

## Status
- Document will be added soon~

```diff
- ***User can use LOS of SFP to disable alignment done.***
- ***Some SFP outputs 0x5555 when without fiber plugged.***
- ***Example: to control alignment_done_f in.***
- ***https://github.com/simpway/GBT4FELIX/blob/master/Firmware/gbt_code/FELIX_gbt_wrapper_V7.vhd***
```
